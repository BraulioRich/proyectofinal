if(process.env.NODE_ENV != 'production'){
    require('dotenv').config()
}

module.exports = {
    database: {
        host: process.env.DB_HOST = "localhost",
        user: process.env.DB_USER = "root",
        password: process.env.DB_PASSWORD = "123456",
        database: process.env.DB_DB = "cellshopp2"
    }
}