const mysql = require('mysql')
const { database } = require('./keys')
const pool = mysql.createPool(database)

const { promisify } = require('util')

pool.getConnection((err, connection) => {
    if(err){
        if(err.code == 'PROTOCOL_CONNECTION_LOST'){
            console.log('La conexion a la base de datos fue cerrada')
        }
        if(err.code == 'ER_CON_COUNT_ERROR'){
            console.log('Db has many connections')
        }
        if(err.code == 'ECONNREFUSED'){
            console.log('Base de datos rechazada')
        }
    }
    if(connection){
        connection.release()
        console.log('Base de datos conectada con exito')
    } 
    return
})

pool.query = promisify(pool.query)

module.exports = pool
